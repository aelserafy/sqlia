package org.sqlia.model;

public abstract class Node{
	
	private String content;

	public Node(String content) {
		this.content = content;
	}
	
	public String getContent() {
		return content;
	}
	
	@Override
	public String toString() {
		return content;
	}
}
