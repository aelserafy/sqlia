package org.sqlia.model;

import java.util.ArrayList;
import java.util.List;

public class KeywordNode extends Node{

	private SQL keyword;
	private List<Node> children = new ArrayList<Node>();
	
	public enum SQL{
		SELECT, UNION, DISTINCT, ORDER_BY, WITH, LIMIT, GROUP_BY, HAVING, INTO, WHERE, FROM, ON, JOIN, USING, BETWEEN, IN, IS_NULL, SWITCH, ELSE, WHEN, THEN, EXISTS, NOT, DELETE, UPDATE, SET, INSERT, REPLACE, DROP, TRUNCATE, CREATE_TABLE, 
	}
	
	public KeywordNode(SQL content) {
		super(content.toString());
		keyword = content;
	}

	public List<Node> getChildren() {
		return children;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getContent());
		sb.append("\n{\n");
		for(Node child:children){
			sb.append(child.toString().replace("\n", "\n\t")).append(",\n");
		}
		if(!children.isEmpty()){
			sb.deleteCharAt(sb.length()-2);
		}
		sb.append("}");
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(!(arg0 instanceof KeywordNode)) return false;
		KeywordNode other = (KeywordNode) arg0;
		if(this.keyword != other.keyword) return false;
		if(this.children.size() != other.children.size()) return false;
		for(Node myChild:this.children){
			if(!other.children.contains(myChild)) return false;
		}
		return true;
	}
}
