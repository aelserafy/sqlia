package org.sqlia.model;

public class CustomNode extends Node {
	
	private boolean isDatabaseKeyword;
	
	public CustomNode(Object content, boolean isDatabaseKeyword) {
		super(content.toString());
		this.isDatabaseKeyword = isDatabaseKeyword;
	}
	
	public boolean isDatabaseKeyword() {
		return isDatabaseKeyword;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(!(arg0 instanceof CustomNode)) return false;
		CustomNode other = (CustomNode) arg0;
		
		String myContent = this.getContent();
		String otherContent = other.getContent();
		if("?".equals(myContent) && ! other.isDatabaseKeyword){
			return true;
		}
		if("?".equals(otherContent) && ! this.isDatabaseKeyword){
			return true;
		}
		return myContent.equals(otherContent) && this.isDatabaseKeyword == other.isDatabaseKeyword;
	}
}
