package org.sqlia.ahocorasick;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import com.google.common.collect.SetMultimap;

import eu.crydee.ahocorasick.AhoCorasick;
import eu.crydee.ahocorasick.AhoCorasickHelpers;
import eu.crydee.ahocorasick.Occurrence;

public class AdaptiveAhoCorasick implements Serializable {
	private static final long serialVersionUID = 7394933267095609051L;
	
//	private AToken root;
	
	private int attacks, detections;

	private List<String[]> attacksList;

	transient private AhoCorasick<String> ahoCorasick;
	
	public AdaptiveAhoCorasick() {
		attacksList = new ArrayList<String[]>(Arrays.asList(new String[][]{}));
		ahoCorasick = new AhoCorasick<String>(attacksList);
//		root = new AToken("");
	}
	
	private AhoCorasick<String> getAhoCorasick() {
		if(ahoCorasick == null){
			ahoCorasick = new AhoCorasick<>(attacksList);
		}
		return ahoCorasick;
	}
	
	public void serialize(String path) throws IOException {
		FileOutputStream fileOut = new FileOutputStream(path);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(this);
		out.close();
		fileOut.close();
	}
	
	public static AdaptiveAhoCorasick deserialize(String path) throws IOException, ClassNotFoundException{
			FileInputStream fileIn = new FileInputStream(path);
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        AdaptiveAhoCorasick t = (AdaptiveAhoCorasick) in.readObject();
	        in.close();
	        fileIn.close();
	        t.getAhoCorasick();
			return t;
	}

	public void learn(LinkedList<String> tokens) {
		String[] attack = tokens.toArray(new String[tokens.size()]);
		if(!attacksList.contains(attack)){
			attacksList.add(attack);
		}
//		return root.learn(tokens);
	}
	
	public double check(LinkedList<String> tokens) {
		return getAhoCorasick().search(tokens.toArray(new String[tokens.size()]));
//		return root.check(tokens);
	}

	public int getDetections() {
		return detections;
	}

	public void setDetections(int detections) {
		this.detections = detections;
	}

	public int getAttacks() {
		return attacks;
	}

	public void setAttacks(int attacks) {
		this.attacks = attacks;
	}
}
