package org.sqlia.visitor;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.IntoTableVisitor;

import static org.sqlia.visitor.MainTreater.*;

public class IntoTableTreater implements IntoTableVisitor {

	@Override
	public void visit(Table table) {
		treatTable(table);
	}
}
