package org.sqlia.visitor;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import static org.sqlia.visitor.MainTreater.*;

public class SelectItemTreater implements SelectItemVisitor {

	@Override
	public void visit(AllColumns allColumns) {
		if(allColumns == null) return;
		
		createAndAppend(allColumns, true);
	}

	@Override
	public void visit(AllTableColumns allTableColumns) {
		if(allTableColumns == null) return;
		
		allTableColumns.getTable().accept(fromItemTreater);
	}

	@Override
	public void visit(SelectExpressionItem selectExpressionItem) {
		if(selectExpressionItem == null) return;
		
		createAndAppend(selectExpressionItem.getAlias(), false);
		Expression expression = selectExpressionItem.getExpression();
		if(expression != null) expression.accept(expressionTreater);
	}

}
