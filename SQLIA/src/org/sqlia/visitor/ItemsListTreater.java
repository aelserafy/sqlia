package org.sqlia.visitor;

import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.statement.select.SubSelect;
import static org.sqlia.visitor.MainTreater.*;

public class ItemsListTreater implements ItemsListVisitor {

	@Override
	public void visit(SubSelect subSelect) {
		treatSubSelect(subSelect);
	}

	@Override
	public void visit(ExpressionList expressionList) {
		treatExpressionsList(expressionList);
	}

}
