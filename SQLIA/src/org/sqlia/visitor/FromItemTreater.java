package org.sqlia.visitor;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;

import static org.sqlia.visitor.MainTreater.*;

public class FromItemTreater implements FromItemVisitor {

	@Override
	public void visit(Table table) {
		treatTable(table);
	}

	@Override
	public void visit(SubSelect subSelect) {
		treatSubSelect(subSelect);
	}

	@Override
	public void visit(SubJoin subJoin) {
		if(subJoin == null) return;
		
		treatJoin(subJoin.getJoin());
		
		FromItem left = subJoin.getLeft();
		if(left != null) left.accept(fromItemTreater);
	}

}
