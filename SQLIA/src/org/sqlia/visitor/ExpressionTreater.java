package org.sqlia.visitor;

import java.util.List;

import org.sqlia.model.KeywordNode;
import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SubSelect;

import static org.sqlia.visitor.MainTreater.*;

public class ExpressionTreater implements ExpressionVisitor {

	@Override
	public void visit(NullValue value) {
		treatUserValue(value);
	}

	@Override
	public void visit(Function function) {
		if(function == null) return;
		
		if(function.isEscaped()) createAndAppend("{", false);
		
		createAndAppend(function.getName(), true);
		
		treatExpressionsList(function.getParameters());
		
		if(function.isEscaped()) createAndAppend("}", false);
	}

	@Override
	public void visit(InverseExpression inverseExpression) {
		if(inverseExpression == null) return;
		
		createAndAppend("-", false);
		
		treatExpression(inverseExpression.getExpression(), null);
	}

	@Override
	public void visit(JdbcParameter value) {
		treatUserValue(value);
	}

	@Override
	public void visit(DoubleValue value) {
		treatUserValue(value);
	}

	@Override
	public void visit(LongValue value) {
		treatUserValue(value);
	}

	@Override
	public void visit(DateValue value) {
		if(value == null) return;
		
		treatUserValue(value.getValue());
	}

	@Override
	public void visit(TimeValue value) {
		if(value == null) return;
		
		treatUserValue(value.getValue());
	}

	@Override
	public void visit(TimestampValue value) {
		if(value == null) return;
		
		treatUserValue(value.getValue());
	}

	@Override
	public void visit(Parenthesis parenthesis) {
		if(parenthesis == null) return;
		
		KeywordNode node = null;
		
		if(parenthesis.isNot()) {
			node = createAndAppend(SQL.NOT);
		}
		
		createAndAppend("(", false);
		restore(node);
		
		treatExpression(parenthesis.getExpression(), null);
		restore(node);
		
		createAndAppend(")", false);
		restore(node);
	}

	@Override
	public void visit(StringValue value) {
		treatUserValue(value);
	}

	@Override
	public void visit(Addition operation) {
		treatBinaryExpression(operation);
	}


	@Override
	public void visit(Division operation) {
		treatBinaryExpression(operation);

	}

	@Override
	public void visit(Multiplication operation) {
		treatBinaryExpression(operation);

	}

	@Override
	public void visit(Subtraction operation) {
		treatBinaryExpression(operation);

	}

	@Override
	public void visit(AndExpression operation) {
		treatBinaryExpression(operation);

	}

	@Override
	public void visit(OrExpression operation) {
		treatBinaryExpression(operation);

	}

	@Override
	public void visit(Between between) {
		if(between == null) return;
		
		if(between.isNot()) createAndAppend(SQL.NOT);
		
		KeywordNode node = createAndAppend(SQL.BETWEEN);
		
		treatExpression(between.getLeftExpression(), null);
		restore(node);
		
		treatExpression(between.getBetweenExpressionStart(), null);
		restore(node);
		
		createAndAppend("AND", false);
		restore(node);
		
		treatExpression(between.getBetweenExpressionEnd(), null);
		restore(node);
	}

	@Override
	public void visit(EqualsTo operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(GreaterThan operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(GreaterThanEquals operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(InExpression inExpression) {
		if(inExpression == null) return;
		
		if(inExpression.isNot()) createAndAppend(SQL.NOT);
		
		KeywordNode node = createAndAppend(SQL.IN);
		
		treatExpression(inExpression.getLeftExpression(), null);
		restore(node);
		
		inExpression.getItemsList().accept(itemsListTreater);
		restore(node);
	}

	@Override
	public void visit(IsNullExpression isNullExpression) {
		if(isNullExpression == null) return;
		
		KeywordNode node = null;
		
		if(isNullExpression.isNot()){
			node = createAndAppend(SQL.NOT);
		}
		
		treatExpression(isNullExpression.getLeftExpression(), SQL.IS_NULL);
		restore(node);
	}

	@Override
	public void visit(LikeExpression likeExpression) {
		if(likeExpression == null) return;
		treatBinaryExpression(likeExpression);
		createAndAppend(likeExpression.getEscape(), false);
	}

	@Override
	public void visit(MinorThan operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(MinorThanEquals operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(NotEqualsTo operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(Column column) {
		if(column == null) return;
		
		treatTable(column.getTable());
		createAndAppend(column.getWholeColumnName(), false);
	}

	@Override
	public void visit(SubSelect subSelect) {
		treatSubSelect(subSelect);
	}

	@Override
	public void visit(CaseExpression caseExpression) {
		if(caseExpression == null) return;
		
		KeywordNode node = createAndAppend(SQL.SWITCH);
		
		treatExpression(caseExpression.getSwitchExpression(), null);
		restore(node);
		
		treatWhenClauses(caseExpression.getWhenClauses());
		restore(node);
		
		Expression elseExpression = caseExpression.getElseExpression();		
		if(elseExpression != null) treatExpression(elseExpression, SQL.ELSE);
		restore(node);
	}

	private void treatWhenClauses(List<WhenClause> whenClauses) {
		if(whenClauses == null) return;
		
		KeywordNode node = createAndAppend(SQL.WHEN);
		
		for(WhenClause whenClause:whenClauses){
			whenClause.accept(expressionTreater);
			restore(node);
		}
	}

	@Override
	public void visit(WhenClause whenClause) {
		if(whenClause == null) return;
		
		KeywordNode node = currentNode;
		treatExpression(whenClause.getWhenExpression(), SQL.WHEN);
		restore(node);
		treatExpression(whenClause.getThenExpression(), SQL.THEN);
		restore(node);
	}

	@Override
	public void visit(ExistsExpression existsExpression) {
		if(existsExpression == null) return;
		
		KeywordNode node = currentNode;
		
		if(existsExpression.isNot()) createAndAppend(SQL.NOT);
		treatExpression(existsExpression.getRightExpression(), SQL.EXISTS);
		restore(node);
	}

	@Override
	public void visit(AllComparisonExpression allComparisonExpression) {
		if(allComparisonExpression == null) return;
		createAndAppend("ALL", false);
		treatSubSelect(allComparisonExpression.GetSubSelect());
	}

	@Override
	public void visit(AnyComparisonExpression anyComparisonExpression) {
		if(anyComparisonExpression == null) return;
		createAndAppend("ANY", false);
		treatSubSelect(anyComparisonExpression.GetSubSelect());
	}

	@Override
	public void visit(Concat concat) {
		treatBinaryExpression(concat);
	}

	@Override
	public void visit(Matches matches) {
		treatBinaryExpression(matches);
	}

	@Override
	public void visit(BitwiseAnd operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(BitwiseOr operation) {
		treatBinaryExpression(operation);
	}

	@Override
	public void visit(BitwiseXor operation) {
		treatBinaryExpression(operation);
	}
	
	private void treatUserValue(Object value) {
		if(value == null) return;
		
		createAndAppend(value, false);
	}
	
	private void treatBinaryExpression(BinaryExpression expression) {
		if(expression == null) return;
		
		KeywordNode node = null;
		
		if(expression.isNot()) {
			node = createAndAppend(SQL.NOT);
		}
		
		treatExpression(expression.getLeftExpression(), null);
		restore(node);
		
		createAndAppend(expression.getStringExpression(), false);
		restore(node);
		
		treatExpression(expression.getRightExpression(), null);
		restore(node);
	}

}
