package org.sqlia.visitor;

import java.util.List;

import org.sqlia.model.KeywordNode;
import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.Distinct;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.Limit;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.Top;
import net.sf.jsqlparser.statement.select.Union;

import static org.sqlia.visitor.MainTreater.*;

public class SelectTreater implements SelectVisitor {
	

	@Override
	public void visit(PlainSelect plainSelect) {
		KeywordNode node = createAndAppend(SQL.SELECT);

		treatDistinct(plainSelect.getDistinct());
		restore(node);
		
		treatSelectItems(plainSelect.getSelectItems(), null);
		restore(node);
		
		treatFromItem(plainSelect.getFromItem());
		restore(node);
		
		treatExpression(plainSelect.getWhere(), SQL.WHERE);
		restore(node);
		
		treatExpression(plainSelect.getHaving(), SQL.HAVING);
		restore(node);
		
		treatOrderByElementList(plainSelect.getOrderByElements());
		restore(node);
		
		treatExpressions(plainSelect.getGroupByColumnReferences(), SQL.GROUP_BY);
		restore(node);
		
		treatJoins(plainSelect.getJoins());
		restore(node);
		
		treatInto(plainSelect.getInto());
		restore(node);
		
		treatLimit(plainSelect.getLimit());
		restore(node);
		
		treateTop(plainSelect.getTop());
		restore(node);
	}

	private void treatJoins(List<Join> joins) {
		if(joins == null) return;
		
		KeywordNode node = createAndAppend(SQL.JOIN);
		
		for(Join join:joins){
			treatJoin(join);
			restore(node);
		}
	}

	private void treateTop(Top top) {
		if(top == null) return;
		
		createAndAppend(top.getRowCount(), false);
	}

	private void treatInto(Table into) {
		if(into == null) return;
		
		KeywordNode node = createAndAppend(SQL.INTO);
		
		into.accept(intoTableTreater);
		restore(node);
	}

	@Override
	public void visit(Union union) {
		KeywordNode node = createAndAppend(SQL.UNION);

		treatPlainSelects(union.getPlainSelects());
		restore(node);

		treatOrderByElementList(union.getOrderByElements());
		restore(node);

		treatLimit(union.getLimit());
		restore(node);
	}
	

	
	private void treatOrderByElementList(List<OrderByElement> orderByElements) {
		if(orderByElements == null) return;
		
		KeywordNode node = createAndAppend(SQL.ORDER_BY);
		
		for(OrderByElement element:orderByElements){
			element.accept(orderByTreater );
			restore(node);
		}
	}
	
	private void treatDistinct(Distinct distinct) {
		if(distinct == null) return;
		List<SelectItem> onSelectItems = distinct.getOnSelectItems();
		if(onSelectItems == null) return;
		
		KeywordNode node = createAndAppend(SQL.DISTINCT);
		
		for(SelectItem onSelectItem:onSelectItems){
			onSelectItem.accept(selectItemTreater);
			restore(node);
		}
	}
	
	private void treatPlainSelects(List<PlainSelect> plainSelects) {
		if(plainSelects == null) return;
		
		//TODO createAndAppend ??
		
		for(PlainSelect plainSelect:plainSelects){
			plainSelect.accept(this);
		}
	}
	
	private void treatLimit(Limit limit) {
		if(limit == null) return;
		
		KeywordNode node = createAndAppend(SQL.LIMIT);
		
		if(limit.isLimitAll()) createAndAppend("ALL", false);
		restore(node);
		if(limit.isOffsetJdbcParameter()) createAndAppend("OffsetJdbcParameter", false); //TODO
		restore(node);
		if(limit.isRowCountJdbcParameter()) createAndAppend("RowCountJdbcParameter", false); //TODO
		restore(node);
		
		createAndAppend(limit.getOffset(), false);
		restore(node);
		createAndAppend(limit.getRowCount(), false);
		restore(node);
	}
}
