package org.sqlia.visitor;

import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.OrderByVisitor;
import static org.sqlia.visitor.MainTreater.*;

public class OrderByTreater implements OrderByVisitor {

	@Override
	public void visit(OrderByElement orderByElement) {
		if(orderByElement == null) return;
		Expression expression = orderByElement.getExpression();
		createAndAppend((orderByElement.isAsc())? "ASC" : "DESC", false);
		if(expression == null) return;
		expression.accept(expressionTreater);
	}

}
