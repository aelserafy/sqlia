package org.sqlia.visitor;

import static org.sqlia.visitor.MainTreater.createAndAppend;
import static org.sqlia.visitor.MainTreater.currentNode;
import static org.sqlia.visitor.MainTreater.expressionTreater;
import static org.sqlia.visitor.MainTreater.itemsListTreater;
import static org.sqlia.visitor.MainTreater.restore;
import static org.sqlia.visitor.MainTreater.rootNode;
import static org.sqlia.visitor.MainTreater.selectTreater;
import static org.sqlia.visitor.MainTreater.treatColumns;
import static org.sqlia.visitor.MainTreater.treatExpressions;
import static org.sqlia.visitor.MainTreater.treatSelectItems;
import static org.sqlia.visitor.MainTreater.treatTable;

import java.util.List;

import org.sqlia.model.KeywordNode;
import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

public class StatementTreater implements StatementVisitor {

	public StatementTreater() {
		rootNode = null;
		currentNode = null;
	}
	
	@Override
	public void visit(Select select) {
		rootNode = new KeywordNode(SQL.SELECT);
		currentNode = rootNode;
		select.getSelectBody().accept(selectTreater);
		restore(rootNode);
		treatSelectItems(select.getWithItemsList(), SQL.WITH);
		restore(rootNode);
	}

	@Override
	public void visit(Delete delete) {
		rootNode = new KeywordNode(SQL.DELETE);
		currentNode = rootNode;
		if(delete == null) return;
		treatTable(delete.getTable());
		restore(rootNode);
		KeywordNode node = createAndAppend(SQL.WHERE);
		delete.getWhere().accept(expressionTreater);
		restore(node);
		restore(rootNode);
	}

	@Override
	public void visit(Update update) {
		rootNode = new KeywordNode(SQL.UPDATE);
		currentNode = rootNode;
		if(update == null) return;
		treatTable(update.getTable());
		restore(rootNode);
		treatColumns(update.getColumns(), SQL.SET);
		restore(rootNode);
		treatExpressions(update.getExpressions(), SQL.SET);
		restore(rootNode);
		KeywordNode node = createAndAppend(SQL.WHERE);
		update.getWhere().accept(expressionTreater);
		restore(node);
		restore(rootNode);
	}

	@Override
	public void visit(Insert insert) {
		rootNode = new KeywordNode(SQL.INSERT);
		currentNode = rootNode;
		if(insert == null) return;
		KeywordNode node = createAndAppend(SQL.INTO);
		treatTable(insert.getTable());
		treatColumns(insert.getColumns(), null);
		restore(rootNode);
		insert.getItemsList().accept(itemsListTreater);
		restore(rootNode);
		if(insert.isUseValues()) createAndAppend("UseValues", false);
		restore(rootNode);
	}

	@Override
	public void visit(Replace replace) {
		rootNode = new KeywordNode(SQL.REPLACE);
		currentNode = rootNode;
		if(replace == null) return;
		treatTable(replace.getTable());
		treatColumns(replace.getColumns(), SQL.SET);
		restore(rootNode);
		treatExpressions(replace.getExpressions(), SQL.SET);
		restore(rootNode);
		replace.getItemsList().accept(itemsListTreater);
		restore(rootNode);
		if(replace.isUseValues()) createAndAppend("UseValues", false);
		restore(rootNode);
	}

	@Override
	public void visit(Drop drop) {
		rootNode = new KeywordNode(SQL.DROP);
		currentNode = rootNode;
		if(drop == null) return;
		String name = drop.getName();
		createAndAppend(name, true);
		restore(rootNode);
		String type = drop.getType();
		createAndAppend(type, true);
		restore(rootNode);
		treatStringList(drop.getParameters());
		restore(rootNode);
	}

	@Override
	public void visit(Truncate truncate) {
		rootNode = new KeywordNode(SQL.TRUNCATE);
		currentNode = rootNode;
		if(truncate == null) return;
		treatTable(truncate.getTable());
		restore(rootNode);
	}

	@Override
	public void visit(CreateTable createTable) {
		rootNode = new KeywordNode(SQL.CREATE_TABLE);
		currentNode = rootNode;
		if(createTable == null) return;
		treateColumnDefinitions(createTable.getColumnDefinitions());
		restore(rootNode);
		treatIndexes(createTable.getIndexes());
		restore(rootNode);
		treatTable(createTable.getTable());
		restore(rootNode);
		treatStringList(createTable.getTableOptionsStrings());
		restore(rootNode);
	}

	private void treatStringList(List<String> strings) {
		if(strings == null) return;
		KeywordNode node = currentNode;
		for(String string:strings){
			createAndAppend(string, true);
			restore(node);
		}
	}

	private void treatIndexes(List<Index> indexes) {
		if(indexes == null) return;
		KeywordNode node = currentNode;
		for(Index index:indexes){
			createAndAppend(index.getName(), true);
			restore(node);
			createAndAppend(index.getType(), true);
			restore(node);
			treatStringList(index.getColumnsNames());
			restore(node);
		}
		
	}

	private void treateColumnDefinitions(List<ColumnDefinition> columnDefinitions) {
		if(columnDefinitions == null) return;
		for(ColumnDefinition columnDefinition:columnDefinitions){
			createAndAppend(columnDefinition.getColumnName(), true);
			KeywordNode node = currentNode;
			
			ColDataType colDataType = columnDefinition.getColDataType();
			if(colDataType != null){
				createAndAppend(colDataType.getDataType(), true);
				restore(node);
				treatStringList(colDataType.getArgumentsStringList());
				restore(node);
			}
			restore(node);
			
			treatStringList(columnDefinition.getColumnSpecStrings());
			restore(node);
		}
	}

}
