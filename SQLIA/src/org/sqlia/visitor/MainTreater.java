package org.sqlia.visitor;

import java.util.List;

import org.sqlia.model.CustomNode;
import org.sqlia.model.KeywordNode;
import org.sqlia.model.KeywordNode.SQL;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SubSelect;

public abstract class MainTreater {
	protected static KeywordNode rootNode;
	protected static KeywordNode currentNode; //TODO replace every static with a master object
	
	protected static SelectTreater selectTreater = new SelectTreater();
	protected static SelectItemTreater selectItemTreater = new SelectItemTreater();
	protected static OrderByTreater orderByTreater = new OrderByTreater();
	protected static ExpressionTreater expressionTreater = new ExpressionTreater();
	protected static IntoTableTreater intoTableTreater = new IntoTableTreater();
	protected static FromItemTreater fromItemTreater = new FromItemTreater();
	protected static ItemsListTreater itemsListTreater = new ItemsListTreater();
	
	protected static void createAndAppend(Object data, boolean dbKeyword) {
		if(data == null) return;
		CustomNode node = new CustomNode(data, dbKeyword);
		currentNode.getChildren().add(node);
	}
	
	protected static KeywordNode createAndAppend(SQL keyword) {
		if(keyword == null) return currentNode;
		KeywordNode parentNode = currentNode;
		currentNode = new KeywordNode(keyword);
		KeywordNode backup = currentNode;
		parentNode.getChildren().add(currentNode);
		return backup;
	}
	
	protected static void restore(KeywordNode node){
		if(node == null) return;
		currentNode = node;
	}
	
	protected static void treatSelectItems(List<SelectItem> selectItems, SQL parentKeyword) {
		if(selectItems == null) return;
		
		createAndAppend(parentKeyword);
		
		for(SelectItem selectItem:selectItems){
			selectItem.accept(selectItemTreater);
		}
	}
	
	protected static void treatJoin(Join join) {
		if(join == null) return;
		
		if(join.isFull()) createAndAppend("FULL", false);
		if(join.isInner()) createAndAppend("INNER", false);
		if(join.isOuter()) createAndAppend("OUTER", false);
		if(join.isLeft()) createAndAppend("LEFT", false);
		if(join.isRight()) createAndAppend("RIGHT", false);
		if(join.isNatural()) createAndAppend("NATURAL", false);
		if(join.isSimple()) createAndAppend("SIMPLE", false);
		
		treatExpression(join.getOnExpression(), SQL.ON);
		treatFromItem(join.getRightItem());
		treatColumns(join.getUsingColumns(), SQL.USING);
	}
	
	protected static void treatColumns(List<Column> usingColumns, SQL keyword) {
		if(usingColumns == null) return;
		
		KeywordNode node = createAndAppend(keyword);
		
		for(Column col:usingColumns){
			col.accept(expressionTreater);
			restore(node);
		}
	}
	
	protected static void treatExpression(Expression expression, SQL parentKeyword) {
		if(expression == null) return;
		
		KeywordNode node = createAndAppend(parentKeyword);
		expression.accept(expressionTreater);
		restore(node);
	}
	
	protected static void treatFromItem(FromItem fromItem) {
		if(fromItem == null) return;
		
		KeywordNode node = createAndAppend(SQL.FROM);
		fromItem.accept(fromItemTreater);
		restore(node);
	}
	
	protected static void treatSubSelect(SubSelect subSelect) {
		if(subSelect == null) return;
		
		createAndAppend(subSelect.getAlias(), true);
		
		SelectBody selectBody = subSelect.getSelectBody();
		if(selectBody != null) selectBody.accept(selectTreater);
	}
	
	protected static void treatTable(Table table) {
		if(table == null) return;
		
		String alias = table.getAlias();
		String tableName = table.getWholeTableName();
		if(alias != null) createAndAppend("Alias: " + alias, true);
		if(tableName != null) createAndAppend("Table_Name: " + tableName, true);
	}
	
	protected static void treatExpressions(List<Expression> expressions, SQL parentKeyword) {
		if(expressions == null) return;
		
		KeywordNode node = createAndAppend(parentKeyword);
		
		for(Expression expression:expressions){
			expression.accept(expressionTreater);
			restore(node);
		}
	}
	
	protected static void treatExpressionsList(ExpressionList expressionList) {
		if(expressionList == null) return;
		
		treatExpressions(expressionList.getExpressions(), null);
	}
	
	public static KeywordNode getRootNode() {
		return rootNode;
	}
}
