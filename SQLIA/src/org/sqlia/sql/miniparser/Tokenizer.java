package org.sqlia.sql.miniparser;

import java.io.StringReader;
import java.util.LinkedList;

public class Tokenizer implements SQLParserConstants{
	private SQLParserTokenManager token_source;

	private enum Type{
		KEYWORD, OPERATOR, TEXT, NUMBER
	}
	
	private Tokenizer(String text) {
		StringReader stream = new StringReader(text);
		SQLParser sqlParser = new SQLParser(stream);
		token_source = sqlParser.token_source;
	}
	
	private Token getNextToken(){
		return token_source.getNextToken();
	}
	
	private static Type getType(Token token){
		switch (token.kind) {
		case NUMBER:
			return Type.NUMBER;
		default:
			if(token.kind >= 5 && token.kind <= 914){
				return Type.KEYWORD;
			}
			if(token.kind >= 925 && token.kind <= 975){
				return Type.OPERATOR;
			}
		}
		return Type.TEXT;
	}
	
	public static LinkedList<String> tokenize(String statement) {
		Tokenizer tokenizer = new Tokenizer(statement);
		LinkedList<String> stringTokens = new LinkedList<String>();
		Token token;
		while((token = tokenizer.getNextToken()).kind != EOF){
			switch(getType(token)){
			case NUMBER:
				stringTokens.add("_NUMBER_");
				break;
			case TEXT:
				stringTokens.add("_TEXT_");
				break;
			case KEYWORD:
			case OPERATOR:
				stringTokens.add(token.toString());
				break;
			}
		}
		return stringTokens;
	}
	
	public static void main(String[] args) {
		String sql = "SELECT * FROM myTable WHERE username = ADMIN AND password = XXX OR 1 = 1;";
		System.out.println(tokenize(sql));
		System.out.println(tokenize(sql));
		System.out.println(tokenize(sql));
		System.out.println(tokenize(sql));
	}
}
