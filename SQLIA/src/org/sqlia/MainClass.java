package org.sqlia;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.sqlia.ahocorasick.AdaptiveAhoCorasick;
import org.sqlia.model.KeywordNode;
import org.sqlia.sql.miniparser.TokenMgrError;
import org.sqlia.sql.miniparser.Tokenizer;
import org.sqlia.visitor.MainTreater;
import org.sqlia.visitor.StatementTreater;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;

public class MainClass {

	private static final String FORCE_LEARNING_MODE = "-f";
	private static final String MARKER_STRING = "?";
	private static final double AHO_CORASICK_NOT_ATTACK_THRESHOLD = 30.0;
	private static final double AHO_CORASICK_ATTACK_THRESHOLD = 70.0;
	private static double AHO_CORASICK_MATURITY_THRESHOLD = 0.8;
	private static final String LOG_FILE = "log.csv";
	private static final String AHO_CORASICK_DATABASE_PATH = "attacks.bin";

	private static Long parseTree_time, ahocorasick_time;

	private static FileWriter writer;
	private static Boolean isMature;
	private static boolean enteredGrayArea;
	private static int detections;
	private static int attacks;

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		if(args.length < 1){
			System.exit(-1);
		}
		List<String> list = new ArrayList<String>(Arrays.asList(args));
		if(list.contains(FORCE_LEARNING_MODE)){
			AHO_CORASICK_MATURITY_THRESHOLD = 1.0;
			list.remove(FORCE_LEARNING_MODE);
		}
		boolean attack = isAttack(list.toArray(new String[list.size()]));
		System.exit(attack ? 1 : 0);
	}

	public static boolean isAttack(String[] args) throws IOException, ClassNotFoundException {
		createLogCsvWriter();
		double ahocorasickPercentage = ahoCorasickAlgorithm(args);
		boolean ahoCorasick_isAttack = (ahocorasickPercentage > AHO_CORASICK_ATTACK_THRESHOLD);
		
		String constructedQuery = constructQuery(args, -1);
		boolean parseTreeAlgorithm_isAttack = parseTreeAlgorithm(args[0], constructedQuery);
		writeLine(ahoCorasick_isAttack, parseTreeAlgorithm_isAttack, constructedQuery, ahocorasickPercentage);
		
		writer.close();
		return ahoCorasick_isAttack;
	}

	/**
	 * Perform ParseTree Algorithm to compare between the original query and the
	 * modified query
	 * 
	 * @param originalQuery
	 * @param modifiedQuery
	 * @return
	 */
	private static boolean parseTreeAlgorithm(String originalQuery, String modifiedQuery) {
		long start = getNow();
		boolean isAttack;
		KeywordNode modifiedRootNode;
		KeywordNode originalRootNode;
		try {
			originalRootNode = parseQuery(originalQuery);
		} catch (JSQLParserException e) {
			throw new RuntimeException("INVALID STATEMENT: " + originalQuery);
		}
		try {
			modifiedRootNode = parseQuery(modifiedQuery);
			isAttack = !originalRootNode.equals(modifiedRootNode);
		} catch (JSQLParserException e) {
			isAttack = true;
		}
		parseTree_time = getNow() - start;
		return isAttack;
	}

	public static KeywordNode parseQuery(String originalQuery) throws JSQLParserException {
		Statement originalStatement;
		originalStatement = new CCJSqlParserManager().parse(new StringReader(originalQuery));
		originalStatement.accept(new StatementTreater());
		KeywordNode originalRootNode = MainTreater.getRootNode();
		return originalRootNode;
	}

	/**
	 * Constructs query from user inputs
	 * 
	 * @param args
	 * @param index
	 *            The order of the desired user input to be replaced. One based.
	 *            -1 for full replacement
	 * @return The constructed query
	 */
	private static String constructQuery(String[] args, int index) {
		String originalQuery = args[0];
		int markersCount = originalQuery.length() - originalQuery.replace(MARKER_STRING, "").length();
		if (markersCount + 1 != args.length) {
			throw new IllegalArgumentException("Number of placeholders (?) doesn't match the number of substitutions");
		}
		StringBuilder modifiedQuery = new StringBuilder(originalQuery);
		int markerIndex = 0;
		if (index < 0) {
			for (int i = 1; i < args.length; i++) {
				markerIndex = modifiedQuery.indexOf(MARKER_STRING, markerIndex);
				modifiedQuery.replace(markerIndex, markerIndex + 1, args[i]);
			}
		} else {
			for (int i = 1; i < args.length; i++) {
				markerIndex = modifiedQuery.indexOf(MARKER_STRING, markerIndex + MARKER_STRING.length());
				if (i == index) { // No need for more replacements
					modifiedQuery.replace(markerIndex, markerIndex + 1, args[i]);
					break;
				}
			}
		}
		return modifiedQuery.toString();
	}

	/**
	 * Perform AhoCorasick Algorithm on user inputs, and get the probability of
	 * being an attack
	 * 
	 * @param args
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static double ahoCorasickAlgorithm(String[] args) throws ClassNotFoundException, IOException {
		double totalPercentage = 0.0;
		AdaptiveAhoCorasick ahoCorasick;
		try {
			ahoCorasick = AdaptiveAhoCorasick.deserialize(AHO_CORASICK_DATABASE_PATH);
		} catch (IOException e) {
			ahoCorasick = new AdaptiveAhoCorasick();
		}
		long start = getNow();
		detections = ahoCorasick.getDetections();
		attacks = ahoCorasick.getAttacks();
		isMature = attacks > 0 ? ((double)detections / attacks) > AHO_CORASICK_MATURITY_THRESHOLD : false;
		for (int userInputIndex = 1; userInputIndex < args.length; userInputIndex++) {
			String userInput = args[userInputIndex];
			if (userInput == null) {
				continue;
			}
			LinkedList<String> tokens;
			try {
				tokens = Tokenizer.tokenize(userInput);
			} catch (TokenMgrError e) {
				totalPercentage = 100.0;
				continue;
			}
			
			double ahoCorasickPercentage = ahoCorasick.check(tokens);
			totalPercentage = Math.max(totalPercentage, ahoCorasickPercentage);

			if (ahoCorasickPercentage > AHO_CORASICK_ATTACK_THRESHOLD) {
				// TOO HIGH => Must be an attack!
				detections++;
				attacks++;
			} else if (ahoCorasickPercentage < AHO_CORASICK_NOT_ATTACK_THRESHOLD) {
				// TOO LOW => Cannot be an attack, unless I'm still immature
				if (!isMature) {
					boolean isAttack = ahoCorasick_AskParseTree(args, ahoCorasick, userInputIndex, tokens);
					if (isAttack) {
						attacks++;
						totalPercentage = 100.0;
					}
				}
			} else {
				// GRAY AREA => I must ask ParseTree, just to be sure!
				enteredGrayArea = true;
				boolean isAttack = ahoCorasick_AskParseTree(args, ahoCorasick, userInputIndex, tokens);
				if (isAttack) {
					attacks++;
					totalPercentage = 100.0;
				}
			}
		}
		ahoCorasick.setAttacks(attacks);
		ahoCorasick.setDetections(detections);
		ahocorasick_time = getNow() - start;
		ahoCorasick.serialize(AHO_CORASICK_DATABASE_PATH);
		return totalPercentage;
	}

	public static boolean ahoCorasick_AskParseTree(String[] args, AdaptiveAhoCorasick ahoCorasick, int userInputIndex,
			LinkedList<String> tokens) {
		String modifiedQuery = constructQuery(args, userInputIndex);
		boolean isAttack = parseTreeAlgorithm(args[0], modifiedQuery);
		if (isAttack) {
			// learn the input attack that you didn't detect!
			ahoCorasick.learn(tokens);
		}
		return isAttack;
	}

	private static long getNow() {
		return System.nanoTime();
	}

	public static String getWD() {
		return Paths.get(".").toAbsolutePath().normalize().toString();
	}

	private static void createLogCsvWriter() throws IOException {
		boolean createHeader = true;
		if (new File(LOG_FILE).exists()) {
			createHeader = false;
		}
		writer = new FileWriter(LOG_FILE, true);
		if (createHeader) {
			writer.append("date").append(",").append("ahoCorasick_isAttack").append(",")
					.append("parseTreeAlgorithm_isAttack").append(",").append("query").append(",")
					.append("isMature").append(",")
					.append("ahoCorasick_uncertain").append(",")
					.append("accuracy").append(",")
					.append("ahocorasick_time").append(",")
					.append("parseTree_time").append(System.getProperty("line.separator"));
			writer.flush();
		}
	}

	private static void writeLine(Boolean ahoCorasick_isAttack, Boolean parseTreeAlgorithm_isAttack, String query,
			Double ahocorasickPercentage) throws IOException {
		String date = Calendar.getInstance().getTime().toString();
		writer.append(date).append(",").append(ahoCorasick_isAttack.toString()).append(",")
				.append(parseTreeAlgorithm_isAttack.toString()).append(",\"").append(query.replace("\"", "\"\"")).append("\",")
				.append(isMature.toString()).append(",")
				.append(String.valueOf(enteredGrayArea)).append(",")
				.append(attacks > 0 ? ""+((double)detections / attacks) : "0").append(",")
				.append(ahocorasick_time.toString()).append(",")
				.append(parseTree_time.toString()).append(System.getProperty("line.separator"));
		writer.flush();
	}
}
