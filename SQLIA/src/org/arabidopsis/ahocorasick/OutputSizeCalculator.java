package org.arabidopsis.ahocorasick;

import java.io.Serializable;

public interface OutputSizeCalculator extends Serializable {
	public int calculateSize(Object output);
}
