package org.arabidopsis.ahocorasick;

public class StringOutputSizeCalculator implements OutputSizeCalculator {

	private static final long serialVersionUID = 845149708119431955L;

	@Override
	public int calculateSize(Object output) {
		if (!(output instanceof String)) {
			throw new IllegalStateException(
					"The output class must be java.lang.String");
		} else {
			return ((String) output).length();
		}
	}

}
