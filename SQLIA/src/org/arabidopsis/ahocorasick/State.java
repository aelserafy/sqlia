package org.arabidopsis.ahocorasick;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A state represents an element in the Aho-Corasick tree.
 */
class State implements Serializable {

	private static final long serialVersionUID = -5773620235137320488L;
	
	private int depth;
	private EdgeList edgeList;
	private State fail;
	private Set<Object> outputs;

	public State(int depth) {
		this.depth = depth;
		this.edgeList = new SparseEdgeList();
		this.fail = null;
		this.outputs = new HashSet<Object>();
	}

	public State extend(char c) {
		if (this.edgeList.get(c) != null)
			return this.edgeList.get(c);
		State nextState = new State(this.depth + 1);
		this.edgeList.put(c, nextState);
		return nextState;
	}

	public State extendAll(char[] chars) {
		State state = this;
		for (int i = 0; i < chars.length; i++) {
			if (state.edgeList.get(chars[i]) != null)
				state = state.edgeList.get(chars[i]);
			else
				state = state.extend(chars[i]);
		}
		return state;
	}

	/**
	 * Returns the size of the tree rooted at this State. Note: do not call this
	 * if there are loops in the edgelist graph, such as those introduced by
	 * AhoCorasick.prepare().
	 */
	public int size() {
		char[] keys = edgeList.keys();
		int result = 1;
		for (int i = 0; i < keys.length; i++)
			result += edgeList.get(keys[i]).size();
		return result;
	}

	public State get(char c) {
		State s = this.edgeList.get(c);
		
		// The root state always returns itself when an state for the passed character doesn't exist
		if(s == null && isRoot()) {
			s = this;
		}
		
		return s;
	}

	public void put(char c, State s) {
		this.edgeList.put(c, s);
	}

	public char[] keys() {
		return this.edgeList.keys();
	}

	public State getFail() {
		return this.fail;
	}

	public void setFail(State f) {
		this.fail = f;
	}

	public void addOutput(Object o) {
		this.outputs.add(o);
	}

	public Set<Object> getOutputs() {
		return this.outputs;
	}
	
	public Boolean isRoot() {
		return depth == 0;
	}
	
	public int getDepth() {
		return depth;
	}
}
