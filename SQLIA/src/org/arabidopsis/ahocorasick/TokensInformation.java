package org.arabidopsis.ahocorasick;

import java.util.Collections;
import java.util.List;

public class TokensInformation {

	// The start offsets of the tokens
	List<Integer> starts;

	// The end offsets of the tokens
	List<Integer> ends;

	// This method sets the starts attribute and it sorts it
	public void setStarts(List<Integer> starts) {
		this.starts = starts;
		Collections.sort(this.starts);
	}

	// This method sets the ends attribute and it sorts it
	public void setEnds(List<Integer> ends) {
		this.ends = ends;
		Collections.sort(this.ends);
	}

	/**
	 * This method returns true if the 'start' param is in the 'starts'
	 * attribute and the 'end' param is in the 'ends' attribute.
	 */
	public boolean areValidOffsets(Integer start, Integer end) {
		return starts != null && ends != null
				&& Collections.binarySearch(starts, start) >= 0
				&& Collections.binarySearch(ends, end) >= 0;
	}
}
