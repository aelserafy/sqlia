package org.arabidopsis.ahocorasick;

import java.io.Serializable;

/**
 * Linked list implementation of the EdgeList should be less memory-intensive.
 */
class SparseEdgeList implements EdgeList {
	
	private static final long serialVersionUID = -7137115190091190011L;
	
	private Cons head;

	public SparseEdgeList() {
		head = null;
	}

	public State get(char c) {
		Cons cons = head;
		while (cons != null) {
			if (cons.c == c)
				return cons.s;
			cons = cons.next;
		}
		return null;
	}

	public void put(char c, State s) {
		this.head = new Cons(c, s, head);
	}

	public char[] keys() {
		int length = 0;
		Cons c = head;
		while (c != null) {
			length++;
			c = c.next;
		}
		char[] result = new char[length];
		c = head;
		int j = 0;
		while (c != null) {
			result[j] = c.c;
			j++;
			c = c.next;
		}
		return result;
	}

	static private class Cons implements Serializable {

		private static final long serialVersionUID = 7161279283678706514L;
		
		char c;
		State s;
		Cons next;

		public Cons(char c, State s, Cons next) {
			this.c = c;
			this.s = s;
			this.next = next;
		}
	}

}
