package org.arabidopsis.ahocorasick;

import java.io.Serializable;

/**
 * Simple interface for mapping bytes to States.
 */
interface EdgeList extends Serializable {
	State get(char ch);

	void put(char ch, State state);

	char[] keys();
}
