package eu.crydee.ahocorasick;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import java.io.Serializable;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Generic Aho-Corasick search class.
 *
 * @param <T>
 *            type of the alphabet to use for your searches.
 */
public class AhoCorasick<T> implements Serializable{

	private static final long serialVersionUID = 2995898229068765053L;

	private class State implements Serializable {

		private static final long serialVersionUID = -5531470515176042945L;

		private Integer remainingDistanceToEnd = null;

		private final Map<T, State> goToTransitions = new HashMap<>();
		private final Set<Integer> outputs = new HashSet<>();
		private State failTransition;
		private Optional<State> defaultTransition = Optional.empty();

		public Map<T, State> getGoToTransitions() {
			return Collections.unmodifiableMap(goToTransitions);
		}

		public State goTo(T symbol) {
			if (goToTransitions.containsKey(symbol)) {
				return goToTransitions.get(symbol);
			}
			return defaultTransition.orElse(null);
		}

		public boolean canGoTo(T symbol) {
			return defaultTransition.isPresent() || goToTransitions.containsKey(symbol);
		}

		public void addGoTo(T symbol, State target) {
			goToTransitions.put(symbol, target);
		}

		public void addOutput(Integer patternIndex) {
			outputs.add(patternIndex);
		}

		public void addOutputs(Set<Integer> patternIndeces) {
			outputs.addAll(patternIndeces);
		}

		public Set<Integer> getOutputs() {
			return Collections.unmodifiableSet(outputs);
		}

		public State fail() {
			return failTransition;
		}

		public void setFail(State target) {
			failTransition = target;
		}

		public void setDefault(State state) {
			defaultTransition = Optional.of(state);
		}

		@Override
		public String toString() {
			return goToTransitions.keySet().toString();
		}

		public void setRemainingDistanceToEnd(int remainingDistanceToEnd) {
			if (this.remainingDistanceToEnd == null || remainingDistanceToEnd < this.remainingDistanceToEnd) {
				this.remainingDistanceToEnd = remainingDistanceToEnd;
			}
		}

		public Integer getRemainingDistanceToEnd() {
			return remainingDistanceToEnd;
		}
	}

	private final State root = new State();

	/**
	 * Constructor of the AhoCorasick search automaton.
	 *
	 * @param patterns
	 *            The list of patterns (sequence of symbols) to use during the
	 *            search.
	 */
	public AhoCorasick(List<T[]> patterns) {
		/*
		 * First phase of the construction in the original paper. Builds the
		 * goto graph and assigns some outputs.
		 */
		for (int i = 0, s = patterns.size(); i < s; ++i) {
			T[] pattern = patterns.get(i);
			State current = root;
			for (int j = 0; j < pattern.length; j++) {
				T symbol = pattern[j];
				if (current.canGoTo(symbol)) {
					current = current.goTo(symbol);
				} else {
					State newState = new State();
					current.addGoTo(symbol, newState);
					current = newState;
				}
				current.setRemainingDistanceToEnd(pattern.length - j - 1);
			}
			current.addOutput(i);
		}
		root.setDefault(root);
		/*
		 * Second phase of the construction in the original paper. Builds the
		 * fail function and assigns the rest of the outputs.
		 */
		// initialization: all the depth == 1 nodes get a fail to 0.
		Deque<State> pool = root.goToTransitions.values().stream().collect(Collectors.toCollection(LinkedList::new));
		pool.forEach(s -> s.setFail(root));
		while (!pool.isEmpty()) {
			State currentState = pool.pop();
			for (Map.Entry<T, State> e : currentState.getGoToTransitions().entrySet()) {
				T symbol = e.getKey();
				State nextState = e.getValue();
				pool.addLast(nextState);
				State failState = currentState.fail();
				while (!failState.canGoTo(symbol)) {
					failState = failState.fail();
				}
				failState = failState.goTo(symbol);
				nextState.setFail(failState);
				nextState.addOutputs(failState.getOutputs());
			}
		}
	}

	/**
	 * Entry point of the library.
	 *
	 * Performs the Aho-Corasick search on the text. The result is a multimap
	 * from matched patterns indices to text indices marking the end of matches.
	 * Both indices are calculated starting from 0.
	 *
	 * @param text
	 *            the sequence of symbols to search.
	 * @return a map of patterns indeces to text indices.
	 */
//	public SetMultimap<Integer, Integer> searchPatternToPos(T[] text) {
//		return search(text, true);
//	}

	/**
	 * Entry point of the library.
	 *
	 * Performs the Aho-Corasick search on the text. The result is a multimap
	 * from text indices marking the end of matches to indices of matched
	 * patterns. Both indices are calculated starting from 0.
	 *
	 * @param text
	 *            the sequence of symbols to search.
	 * @return a map of text indices to patterns indices.
	 */
//	public SetMultimap<Integer, Integer> searchPosToPattern(T[] text) {
//		return search(text, false);
//	}

	private double matchingPercentage;

	public double getMatchingPercentage() {
		return matchingPercentage;
	}

	public double search(T[] text) {
		int matchingCount = 0;
		State currentState = root;
		Integer remainingDistanceToEnd = null;
		for (int i = 0, s = text.length; i < s; ++i) {
			T symbol = text[i];
			if(!currentState.canGoTo(symbol)){
				break;
			}
			currentState = currentState.goTo(symbol);
			remainingDistanceToEnd = currentState.getRemainingDistanceToEnd();
			matchingCount++;
		}
		if(remainingDistanceToEnd == null){
			matchingPercentage = 0;
		}else{
			matchingPercentage = (100.0 * matchingCount) / (matchingCount + remainingDistanceToEnd);
		}
		return matchingPercentage;
	}
}
