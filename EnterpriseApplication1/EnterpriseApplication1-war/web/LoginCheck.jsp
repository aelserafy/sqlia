<%-- 
    Document   : LoginCheck
    Created on : Feb 22, 2016, 9:34:21 AM
    Author     : helraffi
--%>

<%@page import="java.io.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  
        String username=request.getParameter("username");
        String query="SELECT * FROM Users WHERE username = ?;";
        //String[] inputs = new String[]{query, username, password};
        Process process = Runtime.getRuntime().exec("java -jar sqlia.jar \"" + query.replace("\"", "\"\"") +"\" \""+ username.replace("\"", "\"\"") + "\"");
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		String line = null;
		while ((line = in.readLine()) != null){
                    System.err.println(line);
                }
        process.waitFor();
        boolean isAttack = process.exitValue() > 0;
        if(isAttack){
            %>
                <h1 style="color: red">ATTACK!!!!</h1>
            <%
        }else{
            %>
                <h1 style="color: green">SAFE :)</h1>
            <%
        }
//        if((username.equals("admin") && password.equals("pass")))
//            {
//            session.setAttribute("username",username);
//            response.sendRedirect("Home.jsp");
//            }
//        else
//            response.sendRedirect("Error.jsp");
        %>
    </body>
</html>